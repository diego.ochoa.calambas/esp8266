#include <ESP8266WiFi.h>
#include <PubSubClient.h>
 
const char* ssid = "UNE_HFC_D860";
const char* password = "AAA0CBF7";
const char* mqttServer = "m16.cloudmqtt.com";
const int mqttPort = 10585;
const char* mqttUser = "tiendita";
const char* mqttPassword = "tiendita2019";

int PIR_Input = 13;

WiFiClient espClient;
PubSubClient client(espClient);
 
void setup() {
  Serial.begin(115200);
  pinMode(PIR_Input,INPUT);
  setup_wifi();
  setup_MQTT();
}

void setup_wifi(){
  
  Serial.println();
  Serial.print("Connecting to ");
  Serial.println(ssid);
  WiFi.begin(ssid, password);
  
  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.println("Connecting to WiFi..");
  }
  Serial.println("Connected to the WiFi network");
  Serial.println("IP address: ");
  Serial.println(WiFi.localIP());
}

void setup_MQTT(){
  client.setServer(mqttServer, mqttPort);
  client.setCallback(callback);
 
  while (!client.connected()) {
    Serial.println("Connecting to MQTT...");
 
    if (client.connect("ESP8266Client", mqttUser, mqttPassword )) {
 
      Serial.println("connected MQTT");  
      Serial.println("Broker: ");
      Serial.println(mqttServer);
    } else {
 
      Serial.print("failed with state ");
      Serial.print(client.state());
      delay(2000);
 
    }
  }
  client.publish("CONECTION", "ACTIVE Tiendita");
  Serial.println("Send connected MQTT"); 
  client.subscribe("CONECTION"); 
  client.subscribe("IN/SENSOR/PIR");
  client.subscribe("IN/SENSOR/PUMP");
  client.subscribe("IN/SENSOR/LIGHT");
  client.subscribe("OUT/ACTUATOR/LIGHT");
  client.subscribe("OUT/ACTUATOR/PUMP");
  
}

void callback(char* topic, byte* payload, unsigned int length) {
 
  Serial.print("Message arrived in topic: ");
  Serial.println(topic);
  
  Serial.print("Message:");
  for (int i = 0; i < length; i++) {
    Serial.print((char)payload[i]);
    
  }
 
  Serial.println();
  Serial.println("-----------------------");

  if(topic=="OUT/ACTUATOR/PUMP"){
    if((char)payload[0] == 1){
      Serial.print("IN/SENSOR/PUMP on");
      client.publish("IN/SENSOR/PUMP", "on");
    }else {
       Serial.print("IN/SENSOR/PUMP off");
       client.publish("IN/SENSOR/PUMP", "off");
    }
    
  }
  else if(topic=="OUT/ACTUATOR/LIGHT"){
    if((char)payload[0] == 1){
      Serial.print("IN/SENSOR/LIGHT on");
      client.publish("IN/SENSOR/LIGHT", "on");
    }else {
       Serial.print("IN/SENSOR/LIGHT off");
       client.publish("IN/SENSOR/LIGHT", "off");
    }
  }
  
 
}

int oldState=0;
void loop() {
  client.loop();
  int State = digitalRead(PIR_Input);
  
  Serial.println(State);
  delay(1000);
  
   if(State != oldState) {
      if(State == 1){
      client.publish("IN/SENSOR/PIR", "Move");
      Serial.println("Motion detected!");
      delay(1000);
      }
      else {
      Serial.println("Motion absent!");
      client.publish("IN/SENSOR/PIR", "No Move");
      delay(1000);
      }
      oldState=State;
   }
}
